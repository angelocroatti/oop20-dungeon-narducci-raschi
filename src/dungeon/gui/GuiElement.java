package dungeon.gui;

import javax.swing.JFrame;

public interface GuiElement {
  void drawPanel(JFrame frame, String text);
}
